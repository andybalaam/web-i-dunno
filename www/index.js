import * as wasm from "web-i-dunno";

wasm.init();

const form = document.getElementById("encode_form");
const ip = document.getElementById("ip");
const confusion = document.getElementById("confusion");
const result = document.getElementById("result");
const result_confusion = document.getElementById("result-confusion");

function handle_submit() {
    let res = wasm.encode(ip.value, confusion.value);
    result.value = res;
    if (res.startsWith("Error: ")) {
        result.style.color = "red";
        result_confusion.innerText = "";
    } else {
        result.style.color = "";
        let conf = wasm.confusion_level(res);
        if (conf == "") {
            result_confusion.innerText = "";
        } else {
            result_confusion.innerText = `Confusion level: ${conf}`;
        }
    }
    return false;
}

function handle_change() {
    result.value = "";
}

form.onsubmit = handle_submit;
ip.oninput = handle_change;
confusion.onchange = handle_change;
