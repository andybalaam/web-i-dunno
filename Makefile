all: run


build:
	cargo fmt
	cargo test
	wasm-pack build


run: build
	cd www && npm install && npm run start


dist: build
	cd www && npm install && npm run build


publish-npm: build
	wasm-pack login
	wasm-pack publish
