use i_dunno::{self, ConfusionLevel};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn init() {
    set_panic_hook();
}

#[wasm_bindgen]
pub fn encode(ip: &str, confusion_level: &str) -> String {
    let confusion_level = match confusion_level {
        "Minimum" => ConfusionLevel::Minimum,
        "Satisfactory" => ConfusionLevel::Satisfactory,
        "Delightful" => ConfusionLevel::Delightful,
        _ => {
            return String::from(
                "Error: Invalid confusion level. \"
                Should be 'Minimum', 'Satisfactory', or 'Delightful'.",
            );
        }
    };

    if let Ok(ip) = ip.parse() {
        i_dunno::encode(ip, confusion_level).unwrap_or(String::from(
            "Error: No I-DUNNO found for this IP \
                at the supplied Confusion Level.",
        ))
    } else {
        String::from("Error: Not an IP address!")
    }
}

#[wasm_bindgen]
pub fn confusion_level(input: &str) -> String {
    match i_dunno::confusion_level(input) {
        Some(ConfusionLevel::Minimum) => String::from("Minimum"),
        Some(ConfusionLevel::Satisfactory) => String::from("Satisfactory"),
        Some(ConfusionLevel::Delightful) => String::from("Delightful"),
        None => String::from(""),
    }
}

pub fn set_panic_hook() {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}
