# Web I-DUNNO

An implementation of
[RFC 8771](https://www.rfc-editor.org/rfc/rfc8771.html)
as a web page and npm module.

To encode your IP in I-DUNNO format, visit:
[andybalaam.gitlab.io/web-i-dunno](https://andybalaam.gitlab.io/web-i-dunno/).

## Development

### Setup

Follow the instructions at
[rustwasm.github.io/docs/book/game-of-life/setup.html](https://rustwasm.github.io/docs/book/game-of-life/setup.html).

### Build

Run tests:

```bash
cargo test
```

Run locally:

```bash
wasm-pack build
cd www
npm install
npm run start
```

Then go to http://localhost:8080/ to view the app locally.  To rebuild leave
the `npm run start` running, and manually re-run `wasm-pack build` after making
changes to Rust code.

### Deploy

```bash
wasm-pack build
cd www
npm install
npm run build
```

This creates code inside `public/` that is ready to be uploaded to your web
space.
